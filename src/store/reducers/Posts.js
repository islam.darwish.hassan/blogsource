import {GET_POSTS ,SLIDER_ID,SLIDER_NEXT,SLIDER_PREV} from "../actions/actionTypes";
let initial_state = {
    Posts: [],
    Sender_id: null,
    Sender_avatar: null,
    First_page: 1,
    First_page_Url: null,
    Last_page: null,
    Last_page_Url: null,
    Next_page: 2,
    Next_page_Url: null,
    Prev_page: null,
    Prev_page_Url: null,
    Total_Pages:0,
    Total_Posts:0,
    Slider_id:0


}
const blogposts = (state = initial_state, action) => {

    switch (action.type) {
        case GET_POSTS:
            return {
                ...state,
                Posts: action.Posts,
                First_page_Url: action.Meta.first_page_url,
                Last_page_Url: action.Meta.last_page_url,
                Next_page_Url: action.Meta.next_page_url,
                Total_Pages: action.Meta.total,
                Current_Page: action.Meta.current_page,
                Next_page: action.Meta.current_page + 1,
                Prev_page_Url: action.Meta.prev_page_url,
                Total_Posts:action.Meta.total

            };
        case SLIDER_ID:
            return{
                ...state,
                SLIDER_ID:action.index
            } 
        case SLIDER_NEXT:
                return{
                    ...state,
                    SLIDER_ID:0
                } 
       case SLIDER_PREV:

            return{
                ...state,
                SLIDER_ID:1
            }       
        default:
            return state;
    }
}
export default blogposts;
