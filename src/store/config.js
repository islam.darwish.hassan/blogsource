import {createStore,combineReducers}from "redux";
import postsReducer from './reducers/Posts';
const rootReducer =combineReducers({
    blogposts:postsReducer
});
const config=()=>{
    return createStore(rootReducer);
}
export default config;