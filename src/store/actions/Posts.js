import {
    GET_POSTS,SLIDER_ID ,SLIDER_PREV,SLIDER_NEXT
} from "./actionTypes";

export const get_posts = (Posts,Meta) => {
    return {
        type: GET_POSTS,
        Posts,
        Meta

    }
};
export const slider_id =(index)=>{
    return {
        type:SLIDER_ID,
        index
}
};
export const slider_next =()=>{
    return {
        type:SLIDER_PREV,
}
};
export const slider_prev =()=>{
    return {
        type:SLIDER_NEXT,
};
}

