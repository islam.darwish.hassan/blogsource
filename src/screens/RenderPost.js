import React, { Component } from 'react';
import { MDBCol, MDBIcon, MDBRow } from 'mdbreact';
 
 class Render_Post extends Component {
  render() {
    const post = this.props.post;
    const tags = this.props.post.tags;
    const blogger = this.props.post.blogger;
    return (
      <MDBCol rest="12" >
        <MDBRow className="h-100">
          <MDBCol rest="6" className=" d-flex align-items-start justify-content-end">
          <div className="sliderinfocontainer">
                <h1 className="slidertitle  text-shadow underline underline--blue">
                  <strong>{post.title}</strong>
                </h1>

                <MDBRow>
                  {tags.map(function (value, index) {
                    return (
                      <div className="blogtagcontainer mt-5" key={index}><strong>{value.name}</strong></div>
                    )
                  }
                  )}
                </MDBRow>
                <MDBRow className="mt-3">
                  <div className="blogger  ">
                    <img
                      src={blogger.avatar}
                      alt=""
                      className="rounded-circle z-depth-1-half "
                    />
                  </div>
                  <div className="d-flex align-items-center " >
                    <MDBCol style={{ paddingTop: 5 }}>
                      <div ><strong style={{ padding: 0 }}>{blogger.name}</strong></div>
                      <div style={{ marginTop: -5 }} ><small >{post.created_at}</small></div>
                    </MDBCol>

                  </div>
                </MDBRow>

              </div> 
          </MDBCol>

          <MDBCol rest="6"
            style={{
              backgroundImage: `url(${post.header_image})`,
              border: "none",
              backgroundSize: 'cover',
              padding: 0,
            }}
          >
            <div className={"text-primary d-flex  py-5 px-3 h-100 night-fade-gradient"}>
            <div className="d-flex align-self-end w-100 justify-content-start">
              
            <MDBCol sm= "4" className="justify-content-around ">
           <MDBIcon onClick={()=>this.props.slider("prev")}  size="8x" icon="chevron-left" />
            <MDBIcon onClick={()=>this.props.slider("next")}   size="8x" icon="chevron-right" />
              </MDBCol>
              <MDBCol sm= "8">
              <strong >{post.body}</strong>
              </MDBCol>
              </div>
            </div>

          </MDBCol>
        </MDBRow>
      </MDBCol>
    );

  }

  
}
export default Render_Post;
