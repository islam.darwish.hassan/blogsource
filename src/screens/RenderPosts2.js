import React, { Component } from 'react';
import RenderPost2 from"./RenderPost2";
import { MDBRow } from 'mdbreact';
import { connect } from 'react-redux';

 class RenderPosts2 extends Component {
   state={posts:[]}
  componentDidUpdate=async(prevProps)=> {
    // Typical usage (don't forget to compare props):
    if (this.props.Posts !== prevProps.Posts) {
      await this.setState({posts:this.props.Posts});
      //console.log(this.state.posts)

    }
  }
// use reminder to skip redancy of data 
  render() {
      return (
      this.state.posts!=null ?
    <div>
        {this.state.posts.map(function( value,index,elements ) {
        var next=elements[index+1];
        if(index%2===0){
        return (
        next!=null ? 
        <MDBRow  key={index} >
        <RenderPost2  post ={value}/>
        <RenderPost2 post ={next}/>
        </MDBRow>
       :
       <MDBRow  key={index}>
       <RenderPost2 post ={value}/>
       </MDBRow>
       
       )}else{
        return(null)
      }
      
      }
      )}
    </div>
    :null
        );
  }
  
}
const mapStateToProps = state => {
  return {
      Posts: state.blogposts.Posts,

  };
}
const mapDispatchToProps = dispatch => {
  return {
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(RenderPosts2);
