import React, { Component } from 'react';
import RenderPost from"./RenderPost";
import { MDBRow } from 'mdbreact';
import { connect } from 'react-redux';
import { slider_id } from '../store/actions';

 class RenderPosts extends Component {
   state={posts:[] ,Slider_id:null}
  componentDidUpdate=async(prevProps)=> {
    // Typical usage (don't forget to compare props):
    if (this.props.Posts !== prevProps.Posts) {
      await this.setState({posts:this.props.Posts});
      //console.log(this.state.posts)

    }
  }
  componentDidMount=()=>{
 this.setState({Slider_id:this.props.Silder_id})

  }

// use reminder to skip redancy of data 
  render() {
      return (
      this.state.posts!==null &&this.state.posts!==[] &&this.state.posts.length!==0?
      <div>
     <MDBRow className="slider" >
     <RenderPost  slider ={this.onChangeSlider.bind(this)}  post ={this.state.posts[this.state.Slider_id]}/>
     </MDBRow>
  </div>
    :null
        );
  }
  
  onChangeSlider=async(action)=> {
    var self = this;
    if(action==="next"){
      if(this.state.Slider_id<this.props.TotalPosts){
        await self.setState({ Slider_id:this.state.Slider_id+1 });
      }
      else{
        await self.setState({ Slider_id:0 });
  
      }
    }
    else if(action==="prev"){
      if(this.state.Slider_id>0){
      await self.setState({ Slider_id:this.state.Slider_id-1 });
      }else{
      await self.setState({ Slider_id:this.props.TotalPosts });
      }
      

    }

    //console.log(this.state.birthday);
  };
  
}

const mapStateToProps = state => {
  return {
      Posts: state.blogposts.Posts,
      Silder_id : state.blogposts.Slider_id,
      TotalPosts:state.blogposts.Total_Posts-1

  };
}
const mapDispatchToProps = dispatch => {
  return {
    Set_slider: (index) => { dispatch(slider_id(index)) },
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(RenderPosts);
