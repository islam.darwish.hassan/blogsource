import React, { Component } from 'react';
import Posts from "./Posts";
import Nav from "./Navs";
export default class HomePage extends Component {
  render() {
    return (
      <div>
      <Nav/>
      <Posts/>
      </div>
    )}
}
