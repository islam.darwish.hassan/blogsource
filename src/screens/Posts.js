import React, { Component } from 'react';
import axios from "axios";
import RenderPosts from './RenderPosts.js';
import RenderPosts2 from "./RenderPosts2.js";
import {
 get_posts
} from "../store/actions/index";
import { connect } from 'react-redux';
import { MDBContainer } from 'mdbreact';

const API = "http://127.0.0.1:80/siliconarena/public/api/";
const API_Version = "v1/";
const PostsLink = "posts";

class Posts extends Component {
    render() {
        return (
            <MDBContainer fluid={true} className="black">
            <RenderPosts posts ={this.props.Posts}/>
            <RenderPosts2 posts ={this.props.Posts}/>
            </MDBContainer>
                    );
    }
    state = {
        posts: []
    }
    componentDidMount = async () => {
        await this.Get_Posts();
    }

    Get_Posts = async () => {
        await axios
            .get(API + API_Version + PostsLink, {
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                },

            })
            .then(res => {
                const Posts = res.data.data.posts;
                const Meta =res.data.data.meta;
                this.props.Set_Posts(Posts,Meta);
                console.log("Posts Got ! ");
            })
            .catch(function (error) {
                if (error.response) {
                    //error handling
                    console.log(error.response.data.error);
                }
            });

    }


}
const mapStateToProps = state => {
    return {
        Posts: state.blogposts.Posts,

    };
}
const mapDispatchToProps = dispatch => {
    return {
        Set_Posts: (Posts,Meta) => { console.log(Posts); dispatch(get_posts(Posts,Meta)) },
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Posts);
