import React, { Component } from 'react';
import { MDBCol,MDBIcon, MDBRow } from 'mdbreact';

export default class Render_Post2 extends Component {
  render() {
    const post = this.props.post;
    const tags = this.props.post.tags;
    const blogger = this.props.post.blogger;
    return (
    <MDBCol  md="6" 
     style={{
             backgroundImage:`url(${post.header_image})`,
             border: "none",
             backgroundSize: 'cover',
             overflow: 'hidden',
             padding:0,
            }}
            > 
                <div className={"text-primary d-flex align-items-center py-5 px-3 blackbg" }>
                 <MDBCol>
                    <div>
                        <h6 className={this.props.Texttheme}>
                            <MDBIcon icon={this.props.MDBIcon} />
                            <strong>{this.props.Cat}</strong>
                        </h6>
                        <h3 className="blogtitle  text-shadow underline underline--blue">
                            <strong>{post.title}</strong>
                        </h3>
                        <p className="pt-3 blogbody">
                            {post.body}
                        </p>

                      <MDBRow>
                      {tags.map(function(value,index){
                        return(
                        <div className="blogtagcontainer"key={index}><strong>{value.name}</strong></div>
                        )}
                      )}
                      </MDBRow>
                      <MDBRow>
                        <div className="blogger  ">
                        <img
                          src={blogger.avatar}
                          alt=""
                          className="rounded-circle z-depth-1-half "
                        />
                      </div>
                      <div className="d-flex align-items-center " >
                        <MDBCol style={{paddingTop:5}}>
                      <div ><small style={{padding:0}}>{blogger.name}</small></div>
                      <div  style={{marginTop:-5}} ><small >{post.created_at}</small></div>
                      </MDBCol>

                      </div>
                      </MDBRow>
                       
                    </div>
                 </MDBCol>
                </div> 
          </MDBCol>
              
        );

  }
}
/* <MDBBtn color="brand-text" rounded size="md" onClick={this.toggle} >
<MDBIcon far icon={this.props.BtnIcon} className="left" />
{this.props.BtnText}
</MDBBtn> */